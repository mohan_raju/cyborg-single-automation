package dev.spring.jdbc.model;

public class Client_Server_Configuration {
	
	private int clientServerId;
	private String clientServerName;
	private String coreIp;
	private int coreAdapterPort;
	private int coreAudiencePort;
	private int coreReporterPort;
	private String coreAdsIp;
	private int coreAdsPort;
	private String singleApiUrl;
	private String singleApiBizappDbIp;
	private String singleApiCoreDbIp;
	private String singleApiAllocatorDbIp;
	private String singleApiBizappDbName;
	private String singleApiCoreAdvertiserDbName;
	private String singleApiCoreChannelDbName;
	private String singleApiCoreAudienceDbName;
	private String singleApiAllocatorDbName;
	private String singleApiAtcUrl;
	private String singleApiAgencyUrl;
	private String singleApiAviatorUrl;
	private String singleApiRestartAllocatorUrl;
	private String singleApiRestartedAllocatorUrl;
	private String status;
	
	public int getClientServerId() {
		return clientServerId;
	}
	
	public void setClientServerId(int clientServerId) {
		this.clientServerId = clientServerId;
	}
	
	public String getClientServerName() {
		return clientServerName;
	}
	
	public void setClientServerName(String clientServerName) {
		this.clientServerName = clientServerName;
	}
	
	public String getCoreIp() {
		return coreIp;
	}
	
	public void setCoreIp(String coreIp) {
		this.coreIp = coreIp;
	}
	
	public int getCoreAdapterPort() {
		return coreAdapterPort;
	}
	
	public void setCoreAdapterPort(int coreAdapterPort) {
		this.coreAdapterPort = coreAdapterPort;
	}
	
	public int getCoreAudiencePort() {
		return coreAudiencePort;
	}
	
	public void setCoreAudiencePort(int coreAudiencePort) {
		this.coreAudiencePort = coreAudiencePort;
	}
	public int getCoreReporterPort() {
		return coreReporterPort;
	}
	
	public void setCoreReporterPort(int coreReporterPort) {
		this.coreReporterPort = coreReporterPort;
	}
	
	public String getCoreAdsIp() {
		return coreAdsIp;
	}
	
	public void setCoreAdsIp(String coreAdsIp) {
		this.coreAdsIp = coreAdsIp;
	}
	
	public int getCoreAdsPort() {
		return coreAdsPort;
	}
	
	public void setCoreAdsPort(int coreAdsPort) {
		this.coreAdsPort = coreAdsPort;
	}
	
	public String getSingleApiUrl() {
		return singleApiUrl;
	}
	
	public void setSingleApiUrl(String singleApiUrl) {
		this.singleApiUrl = singleApiUrl;
	}
	
	public String getSingleApiBizappDbIp() {
		return singleApiBizappDbIp;
	}
	
	public void setSingleApiBizappDbIp(String singleApiBizappDbIp) {
		this.singleApiBizappDbIp = singleApiBizappDbIp;
	}
	
	public String getSingleApiCoreDbIp() {
		return singleApiCoreDbIp;
	}
	
	public void setSingleApiCoreDbIp(String singleApiCoreDbIp) {
		this.singleApiCoreDbIp = singleApiCoreDbIp;
	}
	
	public String getSingleApiAllocatorDbIp() {
		return singleApiAllocatorDbIp;
	}
	
	public void setSingleApiAllocatorDbIp(String singleApiAllocatorDbIp) {
		this.singleApiAllocatorDbIp = singleApiAllocatorDbIp;
	}
	
	public String getSingleApiBizappDbName() {
		return singleApiBizappDbName;
	}
	
	public void setSingleApiBizappDbName(String singleApiBizappDbName) {
		this.singleApiBizappDbName = singleApiBizappDbName;
	}
	
	public String getSingleApiCoreAdvertiserDbName() {
		return singleApiCoreAdvertiserDbName;
	}
	
	public void setSingleApiCoreAdvertiserDbName(String singleApiCoreAdvertiserDbName) {
		this.singleApiCoreAdvertiserDbName = singleApiCoreAdvertiserDbName;
	}
	
	public String getSingleApiCoreChannelDbName() {
		return singleApiCoreChannelDbName;
	}
	
	public void setSingleApiCoreChannelDbName(String singleApiCoreChannelDbName) {
		this.singleApiCoreChannelDbName = singleApiCoreChannelDbName;
	}
	
	public String getSingleApiCoreAudienceDbName() {
		return singleApiCoreAudienceDbName;
	}
	
	public void setSingleApiCoreAudienceDbName(String singleApiCoreAudienceDbName) {
		this.singleApiCoreAudienceDbName = singleApiCoreAudienceDbName;
	}
	
	public String getSingleApiAllocatorDbName() {
		return singleApiAllocatorDbName;
	}
	
	public void setSingleApiAllocatorDbName(String singleApiAllocatorDbName) {
		this.singleApiAllocatorDbName = singleApiAllocatorDbName;
	}
	
	public String getSingleApiAtcUrl() {
		return singleApiAtcUrl;
	}
	
	public void setSingleApiAtcUrl(String singleApiAtcUrl) {
		this.singleApiAtcUrl = singleApiAtcUrl;
	}
	
	public String getSingleApiAgencyUrl() {
		return singleApiAgencyUrl;
	}
	
	public void setSingleApiAgencyUrl(String singleApiAgencyUrl) {
		this.singleApiAgencyUrl = singleApiAgencyUrl;
	}
	
	public String getSingleApiAviatorUrl() {
		return singleApiAviatorUrl;
	}
	
	public void setSingleApiAviatorUrl(String singleApiAviatorUrl) {
		this.singleApiAviatorUrl = singleApiAviatorUrl;
	}
	
	public String getSingleApiRestartAllocatorUrl() {
		return singleApiRestartAllocatorUrl;
	}
	
	public void setSingleApiRestartAllocatorUrl(String singleApiRestartAllocatorUrl) {
		this.singleApiRestartAllocatorUrl = singleApiRestartAllocatorUrl;
	}
	
	public String getSingleApiRestartedAllocatorUrl() {
		return singleApiRestartedAllocatorUrl;
	}
	
	public void setSingleApiRestartedAllocatorUrl(String singleApiRestartedAllocatorUrl) {
		this.singleApiRestartedAllocatorUrl = singleApiRestartedAllocatorUrl;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	

}
