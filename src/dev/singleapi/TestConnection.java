package dev.singleapi;

import java.io.IOException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;



@Path("/connect")
public class TestConnection {	
	
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String sayHelloHTML() throws IOException
	{
		
		String resource="";
		try
		{
			resource="Connected";
		}							
		catch(Exception e)
		{			
			resource=e.getMessage();
		}
		
		return resource;
	}

}
