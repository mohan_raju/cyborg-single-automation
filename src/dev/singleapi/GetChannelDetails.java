package dev.singleapi;



import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import java.util.Properties;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.json.JSONException;
import org.json.JSONObject;

import com.jayway.restassured.response.Headers;





@Path("/GetChannelDetails")
public class GetChannelDetails 
{				
	Connection con=null;
	ApiCaller apiCaller = new ApiCaller();
	Headers headers;
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getChannelDetails(@QueryParam("ChannelId") int channelId)throws SQLException,ClassNotFoundException, IOException, JSONException
	{											
										
		String url = "https://test1.atc.skynet.tv/server/public/index.php/api/v2/channel-details/"+channelId;
		//String url1 = "https://test1.atc.skynet.tv/server/public/index.php/api/v2/channel-list";
		//	String method = "GET";
		JSONObject apiResponse = apiCaller.get(url, headers);
		
		System.out.println(apiResponse.toString());		
		return apiResponse.toString();		
	}
		
/*	public void callApi()
	{
		String method = "";
		String response = "";
		Properties appConfig = null;
		
		try {
			if (method.toUpperCase().compareTo("GET") == 0) {
				response = apiCaller.get();			
			} else if (method.toUpperCase().compareTo("POST") == 0) {
				response = apiCaller.post(appConfig);			
			} else if (method.toUpperCase().compareTo("PUT") == 0) {
				response = apiCaller.put();			
			} else if (method.toUpperCase().compareTo("DELETE") == 0) {
				response = apiCaller.delete(appConfig);			
			} else if (method.toUpperCase().compareTo("PATCH") == 0) {
				response = apiCaller.patch();			
			} else if (method.toUpperCase().compareTo("GET-POLL") == 0) {
				response = apiCaller.getPoll(appConfig);			
			}else if (method.toUpperCase().compareTo("POST-POLL") == 0) {
				response = apiCaller.postPoll(appConfig);			
			}
		} catch (Exception e) {
		
		}
	}      */
	
	
	

}
