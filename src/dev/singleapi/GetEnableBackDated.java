package dev.singleapi;

import com.jayway.restassured.response.Headers;
import com.jayway.restassured.response.Response;
import org.json.JSONException;
import org.json.JSONObject;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.sql.SQLException;

import com.jayway.restassured.response.Header;
import org.json.JSONException;
import org.json.JSONObject;

import com.jayway.restassured.response.Headers;

import static com.jayway.restassured.RestAssured.given;

@Path("/get-enabled-back-dated")
public class GetEnableBackDated {
    ApiCaller apiCaller = new ApiCaller();
    Headers headers;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getEnabledBackDated(@QueryParam("server") String serverName, String inputValueForTest)
            throws SQLException,ClassNotFoundException, IOException, JSONException {
        JSONObject jsonObject = new JSONObject(inputValueForTest);
        JSONObject apiResponseInJson = new JSONObject();
        try{
            String isEnabled = jsonObject.getString("is_enabled");
            String start_date = jsonObject.getString("start_date")+" 00:00:00";
            String end_date = jsonObject.getString("end_date");
            String url = "https://"+serverName+".skynet.tv/server/public/index.php/api/system/config/back-dates/"+isEnabled;
            Response apiResponse = given().formParameter("start_date",start_date).when().post(url);
            System.out.println("Coming inside url="+url);

            int statusCode = apiResponse.getStatusCode();
            if (statusCode >= 200 && statusCode < 300)
            {
                apiResponseInJson.put("status", "PASS");
                apiResponseInJson.put("message", "Enabling back dated was success");
                apiResponseInJson.put("code",200);
                return apiResponseInJson.toString();
            }
            else
            {
                apiResponseInJson.put("status", "FAIL");
                apiResponseInJson.put("message", "Enabling back dated failed");
                apiResponseInJson.put("code",400);
                return apiResponseInJson.toString();
            }
        }catch(Exception e){
            apiResponseInJson.put("status", "FAIL");
            apiResponseInJson.put("message", e.getMessage());
            apiResponseInJson.put("code",400);
            return apiResponseInJson.toString();
        }
    }
}
