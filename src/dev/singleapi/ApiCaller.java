package dev.singleapi;

import static com.jayway.restassured.RestAssured.given;

import java.util.ArrayList;
import java.util.Properties;

import org.json.JSONException;
import org.json.JSONObject;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.config.HttpClientConfig;
import com.jayway.restassured.config.RestAssuredConfig;
import com.jayway.restassured.response.Header;
import com.jayway.restassured.response.Headers;
import com.jayway.restassured.response.Response;


public class ApiCaller {

	String apiResponse;
	JSONObject apiResponseInJson = new JSONObject();
	ArrayList<Header> listOfHeader = new ArrayList<Header>();
	Headers headers;
	
	public JSONObject get(String url, Headers ListOfHeaders ) throws JSONException {
		try
		{								
			
			Header h1 = new Header("content-type", "application/json");
			listOfHeader.add(h1);
			headers = new Headers(listOfHeader);
			RestAssured.urlEncodingEnabled = true;
			Response r = given().headers(headers).when().get(url);
			String body = r.getBody().asString();
			
			int statusCode = r.getStatusCode();			
			if (statusCode >= 200 && statusCode < 300) 
			{			
				apiResponseInJson.put("Status", "PASS");
				apiResponseInJson.put("Value", body);				
				return apiResponseInJson;
			} 
			else 
			{				
				apiResponseInJson.put("Status", "FAIL");
				apiResponseInJson.put("Value", body);							
				return apiResponseInJson;
			}
		}
		catch(Exception e)
		{
			apiResponseInJson.put("Status", "FAIL");
			apiResponseInJson.put("Value", "Exception");	
			return apiResponseInJson;
		}
	}

	///// later methods 
	
	/*
	
	
	public static String getPoll(Properties timeout) throws InterruptedException 
	{

		try
		{			
			int pollTimeout = Integer.valueOf(timeout.getProperty("pollTimeout"));
			int pollingFrequency = Integer.valueOf(timeout.getProperty("pollingFrequency"));
			int pollingFrequencyInMS = pollingFrequency *1000;

			for (int i = 0; i <= pollTimeout; i = i + pollingFrequency)
			{		
				RestAssured.urlEncodingEnabled = true;
				Response getPollResponse = null;
				getPollResponse = given().redirects().follow(false).headers(ListOfHeaders).when().get(url.get());

				String body = getPollResponse.getBody().asString();
				int statusCode = getPollResponse.getStatusCode();
			

				if (statusCode >= 200 && statusCode < 300) 
				{
					ValidResponse.set(Constants.KEYWORD_PASS + "-" +"StatusCode:" + statusCode + " Response:" + body);
					break;					
				}
				else if (statusCode > 300 && statusCode < 400 && i >= pollTimeout)
				{				
					ValidResponse.set(Constants.KEYWORD_FAIL + "-" + "StatusCode:" + statusCode + " Response:" + "Request was not completed in the Expected Time:" + pollTimeout + "secs");// +" Logs available at "+apiLogs.get());									
					break;
				}
				else if (statusCode >= 400) 
				{
					ValidResponse.set(Constants.KEYWORD_FAIL + "-" +"StatusCode:" + statusCode + " Response:" + body);// +" Logs available at "+apiLogs.get() +" Logs available at "+apiLogs.get());
					break;
				}
				else
				{						
					ValidResponse.set(Constants.KEYWORD_FAIL + "-" +"StatusCode:" + statusCode + " Response:" + "Request was not completed in the Expected Time:" + pollTimeout + "secs");// +" Logs available at "+apiLogs.get());
				}	            
				Thread.sleep(pollingFrequencyInMS);
			}	        
			return ValidResponse.get();
		}
		catch(Exception e)
		{
			
			e.printStackTrace();
			return "";        	
		}
	}

	public static String postPoll(Properties timeout) throws InterruptedException 
	{
		try
		{			
			int pollTimeout = Integer.valueOf(timeout.getProperty("pollTimeout"));
			int pollingFrequency = Integer.valueOf(timeout.getProperty("pollingFrequency"));
			int pollingFrequencyInMS = pollingFrequency *1000;
			int responseTimeout = Integer.valueOf(timeout.getProperty("responseTimeout"));
		
			RestAssuredConfig config = RestAssured.config()
					.httpClient(HttpClientConfig.httpClientConfig().setParam("http.socket.timeout", responseTimeout * 1000)
							.setParam("http.connection.timeout", responseTimeout * 1000)
							.setParam("http.connection-manager.timeout", new Long(responseTimeout * 1000))
							.setParam("http.protocol.head-body-timeout", responseTimeout * 1000));

						

			for (int i = 0; i <= pollTimeout; i = i + pollingFrequency) 
			{
			
				RestAssured.urlEncodingEnabled = true;
				Response postResponse = null;

				postResponse = given().config(config).relaxedHTTPSValidation().request().headers(ListOfHeaders)
						.body(requestedJson.get()).when().post(url.get());

				String body = postResponse.getBody().asString();
				int statusCode = postResponse.getStatusCode();
			
				if (statusCode >= 200 && statusCode < 300) 
				{								
					ValidResponse.set(Constants.KEYWORD_PASS + "-" +"StatusCode:" + statusCode + " Response:" + body);					
					break;
				}
				else if (statusCode > 300 && statusCode < 400 && i >= pollTimeout) 
				{
					ValidResponse.set(Constants.KEYWORD_FAIL + "-" + "StatusCode:" + statusCode + " Response:" + "Request was not completed in the Expected Time:" + pollTimeout + "secs");// +" Logs available at "+apiLogs.get());
					break;
				}
				else if (statusCode >= 400) 
				{								
					ValidResponse.set(Constants.KEYWORD_FAIL + "-" +"StatusCode:" + statusCode + " Response:" + body);// +" Logs available at "+apiLogs.get());
					break;
				}
				else
				{				
					ValidResponse.set(Constants.KEYWORD_FAIL + "-" +"StatusCode:" + statusCode + " Response:" + "Request was not completed in the Expected Time:" + pollTimeout + "secs");// +" Logs available at "+apiLogs.get());
				}           
				Thread.sleep(pollingFrequencyInMS);
			}			
			return ValidResponse.get();
		}
		catch(Exception e)
		{			
			return Constants.KEYWORD_FAIL + "-" + "Exception Occured While getting response:" + e.getMessage();// +" Logs available at "+apiLogs.get();
		}
	}

	public static String post(Properties timeout) 
	{
		try 
		{
					

			int responseTimeout = Integer.valueOf(timeout.getProperty("responseTimeout"));
					
			//Setting the TimeOut for the API Call
			RestAssuredConfig config = RestAssured.config()
					.httpClient(HttpClientConfig.httpClientConfig().setParam("http.socket.timeout", responseTimeout * 1000)
							.setParam("http.connection.timeout", responseTimeout * 1000)
							.setParam("http.connection-manager.timeout", new Long(responseTimeout * 1000))
							.setParam("http.protocol.head-body-timeout", responseTimeout * 1000)); 

			RestAssured.urlEncodingEnabled = true;
			Response postResponse = null;

			postResponse = given().config(config).relaxedHTTPSValidation().request().headers(ListOfHeaders)
					.body(requestedJson.get()).when().post(url.get());

			String body = postResponse.getBody().asString();
			int statusCode = postResponse.getStatusCode();
			
			if (statusCode >= 200 && statusCode < 300) 
			{
			
				ValidResponse.set("PASS-StatusCode:" + statusCode + " Response:" + body);
				return ValidResponse.get();
			}
			else 
			{
				ValidResponse.set("FAIL-StatusCode:" + postResponse.getStatusCode() + " Response:" + body);// +" Logs available at "+apiLogs.get());							
				return ValidResponse.get();
			}
		}
		catch (Exception e) 
		{			
			return "";
		}		
	}

	public static String post(JSONObject jsonRequest,String url,Properties timeout) 
	{
		try 
		{	
			int responseTimeout = Integer.valueOf(timeout.getProperty("responseTimeout"));
			
			//Setting the TimeOut for the API Call
			RestAssuredConfig config = RestAssured.config()
					.httpClient(HttpClientConfig.httpClientConfig().setParam("http.socket.timeout", responseTimeout * 1000)
							.setParam("http.connection.timeout", responseTimeout * 1000)
							.setParam("http.connection-manager.timeout", new Long(responseTimeout * 1000))
							.setParam("http.protocol.head-body-timeout", responseTimeout * 1000)); 

			RestAssured.urlEncodingEnabled = true;
			Response postResponse = null;

			//postResponse = given().config(config).relaxedHTTPSValidation().request().body(jsonRequest).when().post(url);
			postResponse = given().config(config).relaxedHTTPSValidation().request().headers(ListOfHeaders).body(jsonRequest.toString()).when().post(url);
			String body = postResponse.getBody().asString();
			

			//MODIFYING THE ORIGINAL COS THE JSON ARRAY HAS TO BE CAPTURED. IF ' "RESPONSE:" ' IS APPENDED IT MAKES IT INVALID FOR JSONARRY
			//ORIGINAL: ValidResponse.set(" Response:" + body);
			ValidResponse.set(body);
		}
		catch (Exception e) 
		{			
			return "";
		}
		return "";
	}

	public static String put()
	{
		try
		{
			// RestAssured.baseURI =url; // --> old				
			Response r = given().headers(ListOfHeaders).body(jsonAsString.get()).when().put(url.get());

			String body = r.getBody().asString();			

			int statusCode = r.getStatusCode();
			if (statusCode >= 200 && statusCode < 300) 
			{
				ValidResponse.set(body);
				return "PASS -StatusCode:" + statusCode + " Response:" + body;
			}
			else
			{
				return "FAIL -StatusCode:" + statusCode + " Response:" + body;// +" Logs available at "+apiLogs.get();
			}
		}
		catch (Exception e) 
		{			
			return Constants.KEYWORD_FAIL + "Response TimeOut";// +" Logs available at "+apiLogs.get();
		}
	}

	public static String patch() 
	{
		try
		{					
			Response r = given().headers(ListOfHeaders).body(jsonAsString.get()).when().patch(url.get());
			String body = r.getBody().asString();		
			int statusCode = r.getStatusCode();
			if (statusCode >= 200 && statusCode < 300) 
			{
				ValidResponse.set(body);
				return "PASS -StatusCode:" + statusCode + " Response:" + body;
			}
			else
			{
				return "FAIL -StatusCode:" + statusCode + " Response:" + body;// +" Logs available at "+apiLogs.get();
			}
		}
		catch(Exception e)
		{		
			return "Exception caught at do patch request";
		}
	}

	public static String doDeleteRequest_Orig() 
	{
		Response r = given().headers(ListOfHeaders).body(jsonAsString.get()).when().delete(url.get());
		String body = r.getBody().asString();
	
		int statusCode = r.getStatusCode();
		if (statusCode >= 200 && statusCode < 300) 
		{
			ValidResponse.set(body);
			return "PASS -StatusCode:" + statusCode + " Response:" + body;

		}
		else 
		{
			return "FAIL -StatusCode:" + statusCode + " Response:" + body;// +" Logs available at "+apiLogs.get();
		}

	}

	public static String delete(Properties prop) 
	{
		try
		{
			int responseTimeout = Integer.valueOf(prop.getProperty("responseTimeout"));
		
			//Setting the TimeOut for the API Call
			RestAssuredConfig config = RestAssured.config()
					.httpClient(HttpClientConfig.httpClientConfig().setParam("http.socket.timeout", responseTimeout * 1000)
							.setParam("http.connection.timeout", responseTimeout * 1000)
							.setParam("http.connection-manager.timeout", new Long(responseTimeout * 1000))
							.setParam("http.protocol.head-body-timeout", responseTimeout * 1000)); 

			RestAssured.urlEncodingEnabled = true;
			Response deleteResponse = null;

			deleteResponse = given().config(config).relaxedHTTPSValidation().request().headers(ListOfHeaders)
					.body(requestedJson.get()).when().delete(url.get());

			String body = deleteResponse.getBody().asString();
			

			int statusCode = deleteResponse.getStatusCode();
			if (statusCode >= 200 && statusCode < 300) 
			{
				ValidResponse.set("PASS-StatusCode:" + statusCode + " Response:" + body);			
				return ValidResponse.get();		
			}
			else 
			{				
				ValidResponse.set("FAIL-StatusCode:" + deleteResponse.getStatusCode() + " Response:" + body);// +" Logs available at "+apiLogs.get());					
				return ValidResponse.get();
			}
		}
		catch (Exception e) 
		{			
			return Constants.KEYWORD_FAIL + "-" + "Exception Occured While getting response:" + e.getMessage();// +" Logs available at "+apiLogs.get();
		}
	}
		
	public String doGetRequest()
	{
		String url = "https://test1.atc.skynet.tv/server/public/index.php/api/v2/channel-details/557";
		String ApiHeader = ""; 
		
		ArrayList<Header> list = new ArrayList<Header>();		 
		 
		 	String[] HeadersArray = ApiHeader.split(",");
			for (int i = 0; i < HeadersArray.length; i = i + 2)
			{
				String key = HeadersArray[i];
				
				String value = HeadersArray[i + 1];
			
				Header h1 = new Header(key, value);
				list.add(h1);
			}
		 
		Headers ListOfHeaders = new Headers(list);
		String validResponse = "";
		
	
		RestAssured.urlEncodingEnabled = true;
		Response r = given().headers(ListOfHeaders).when().get(url);

		String body = r.getBody().asString();


		int statusCode = r.getStatusCode();


		if (statusCode >= 200 && statusCode < 300) 
		{	
			validResponse = "PASS-StatusCode:" + statusCode + " Response:" + body;														
			return validResponse;
		} 
		else 
		{						
			validResponse = "FAIL-StatusCode:" + statusCode + " Response:" + body;												
			return validResponse;
		}
											
	}
	*/
	public String doPostRequest(Properties app)
	{return "";}
	
	public String doPutRequest()
	{return "";}
	
	public String doPatchRequest()
	{return "";}
	
	public String doDeleteRequest(Properties app)
	{return "";}
	
	public String doGetRequestPoll(Properties app)
	{return "";}
	
	public String doPostRequestPoll(Properties app)
	{return "";}
	
	
	
}
